package dwi.bagas.uaskampoengjava

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_menu.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class MenuActivity:AppCompatActivity(), View.OnClickListener {
    lateinit var mediaHelper : MediaHelper
    lateinit var menuAdapter : AdapterDataMenu
    lateinit var satuanAdapter : ArrayAdapter<String>
    lateinit var kategoriAdapter : ArrayAdapter<String>
    var daftarMenu = mutableListOf<HashMap<String,String>>()
    var daftarSatuan = mutableListOf<String>()
    var daftarKategori = mutableListOf<String>()
    val mainUrl = "http://192.168.43.94/uas_android/"
    val url = mainUrl+"show_data.php"
    val url2 = mainUrl+"get_nama_satuan.php"
    val url3 = mainUrl+"query_crud_menu.php"
    val url4 = mainUrl+"get_nama_kategori.php"
    var imStr = ""
    var pilihSatuan = ""
    var pilihKategori = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        menuAdapter = AdapterDataMenu(daftarMenu,this)
        mediaHelper = MediaHelper(this)
        listMenu.layoutManager = LinearLayoutManager(this)
        listMenu.adapter = menuAdapter

        satuanAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,
            daftarSatuan)
        spSatuan.adapter = satuanAdapter
        spSatuan.onItemSelectedListener = itemSelected

        kategoriAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,
            daftarKategori)
        spKategori.adapter = kategoriAdapter
        spKategori.onItemSelectedListener = itemSelected

        imgUpload.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnInsert.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        btnFind.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imgUpload -> {
                val intent = Intent()
                intent.setType("image/*")
                intent.setAction(Intent.ACTION_GET_CONTENT)
                startActivityForResult(intent,mediaHelper.getRcGallery())
            }
            R.id.btnInsert -> {
                queryCrudMenu("insert")
            }
            R.id.btnDelete -> {
                queryCrudMenu("delete")
            }
            R.id.btnUpdate -> {
                queryCrudMenu("update")
            }
            R.id.btnFind -> {
                showDataMenu(edNama.text.toString().trim())
            }
        }
    }

    val itemSelected = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spSatuan.setSelection(0)
            pilihSatuan = daftarSatuan.get(0)
            spKategori.setSelection(0)
            pilihKategori = daftarKategori.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihSatuan = daftarSatuan.get(position)
            pilihKategori = daftarKategori.get(position)
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == mediaHelper.getRcGallery()){
                imStr = mediaHelper.getBitmapToString(data!!.data,imgUpload)
            }
        }
    }

    fun queryCrudMenu(mode : String){
        val request = object : StringRequest(
            Method.POST,url3,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil", Toast.LENGTH_LONG).show()
                    showDataMenu("")
                    clearInput()
                }else{
                    Toast.makeText(this,"Operasi Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                val nmFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
                    .format(Date())+".jpg"
                when(mode){
                    "insert" -> {
                        hm.put("mode","insert")
                        hm.put("id_menu",edIdmenu.text.toString())
                        hm.put("nama",edNama.text.toString())
                        hm.put("harga",edHarga.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",nmFile)
                        hm.put("nama_satuan",pilihSatuan)
                        hm.put("nama_kategori",pilihKategori)
                    }
                    "update" -> {
                        hm.put("mode","update")
                        hm.put("id_menu",edIdmenu.text.toString())
                        hm.put("nama",edNama.text.toString())
                        hm.put("harga",edHarga.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",nmFile)
                        hm.put("nama_satuan",pilihSatuan)
                        hm.put("nama_kategori",pilihKategori)
                    }
                    "delete" -> {
                        hm.put("mode","delete")
                        hm.put("id_menu",edIdmenu.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getNamaSatuan(namaSatuan : String){
        val request = object :StringRequest(
            Request.Method.POST,url2,
            Response.Listener { response ->
                daftarSatuan.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarSatuan.add(jsonObject.getString("nama_satuan"))
                }
                satuanAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("nama_satuan",namaSatuan)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getNamaKategori(namaKategori : String){
        val request = object :StringRequest(
            Request.Method.POST,url4,
            Response.Listener { response ->
                daftarKategori.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarKategori.add(jsonObject.getString("nama_kategori"))
                }
                kategoriAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("nama_kategori",namaKategori)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDataMenu(namaMenu : String){
        val request = object :StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarMenu.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var menu = HashMap<String,String>()
                    menu.put("id_menu",jsonObject.getString("id_menu"))
                    menu.put("nama",jsonObject.getString("nama"))
                    menu.put("nama_satuan",jsonObject.getString("nama_satuan"))
                    menu.put("nama_kategori",jsonObject.getString("nama_kategori"))
                    menu.put("harga",jsonObject.getString("harga"))
                    menu.put("url",jsonObject.getString("url"))
                    daftarMenu.add(menu)
                }
                menuAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("nama",namaMenu)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onStart() {
        super.onStart()
        showDataMenu("")
        getNamaSatuan("")
        getNamaKategori("")
    }
    fun clearInput(){
        edIdmenu.setText("")
        edNama.setText("")
        edHarga.setText("")
    }
}