package dwi.bagas.uaskampoengjava

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_satuan.*

class AdapterSatuan(val dataSatuan : List<HashMap<String,String>>, val satuanActivity: SatuanActivity) :
    RecyclerView.Adapter<AdapterSatuan.HolderDataSatuan>(){

    override fun onCreateViewHolder( p0: ViewGroup, p1: Int): AdapterSatuan.HolderDataSatuan {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_satuan,p0,false)
        return  HolderDataSatuan(v)
    }

    override fun getItemCount(): Int {
        return dataSatuan.size
    }

    override fun onBindViewHolder(holder: AdapterSatuan.HolderDataSatuan, position: Int) {
        val data = dataSatuan.get(position)
        holder.txIdsatuan.setText(data.get("id_satuan"))
        holder.txNamaSatuan.setText(data.get("nama_satuan"))

        holder.cLayout.setOnClickListener({
            satuanActivity.edIdsatuan.setText(data.get("id_satuan"))
            satuanActivity.edNamaSatuan.setText(data.get("nama_satuan"))
        })
    }

    class HolderDataSatuan(v: View) : RecyclerView.ViewHolder(v) {
        val txIdsatuan = v.findViewById<TextView>(R.id.txIdsatuan)
        val txNamaSatuan = v.findViewById<TextView>(R.id.txNamaSatuan)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayout)
    }
}