package dwi.bagas.uaskampoengjava

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_kategori.*
import kotlinx.android.synthetic.main.activity_kategori.btnDelete
import kotlinx.android.synthetic.main.activity_kategori.btnFind
import kotlinx.android.synthetic.main.activity_kategori.btnInsert
import kotlinx.android.synthetic.main.activity_kategori.btnUpdate
import org.json.JSONArray
import org.json.JSONObject
import kotlin.collections.HashMap

class KategoriActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var kategoriAdapter : AdapterKategori
    var daftarKategori = mutableListOf<HashMap<String,String>>()
    var idKategori = ""
    val mainUrl = "http://192.168.43.94/uas_android/"
    val url = mainUrl+"get_nama_kategori.php"
    val url2 = mainUrl+"query_crud_kategori.php"

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsert ->{
                queryKategori("insert")
            }
            R.id.btnUpdate ->{
                queryKategori("update")
            }
            R.id.btnDelete ->{
                queryKategori("delete")
            }
            R.id.btnFind -> {
                showDataKategori(edNamaKategori.text.toString())
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kategori)
        kategoriAdapter = AdapterKategori(daftarKategori,this)
        listKategori.layoutManager = LinearLayoutManager(this)
        listKategori.adapter = kategoriAdapter
        btnInsert.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        btnFind.setOnClickListener(this)
    }

    fun showDataKategori(namaKategori : String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarKategori.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var kategori = HashMap<String,String>()
                    kategori.put("id_kategori",jsonObject.getString("id_kategori"))
                    kategori.put("nama_kategori",jsonObject.getString("nama_kategori"))
                    daftarKategori.add(kategori)
                }
                kategoriAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("nama_kategori",namaKategori)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun queryKategori(mode : String){
        val request = object : StringRequest(
            Method.POST,url2,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil", Toast.LENGTH_LONG).show()
                    showDataKategori("")
                    clearInput()
                }else{
                    Toast.makeText(this,"Operasi Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "update" -> {
                        hm.put("mode","update")
                        hm.put("id_kategori",edIdkategori.text.toString())
                        hm.put("nama_kategori",edNamaKategori.text.toString())
                    }
                    "insert" -> {
                        hm.put("mode","insert")
                        hm.put("id_kategori",edIdkategori.text.toString())
                        hm.put("nama_kategori",edNamaKategori.text.toString())
                    }
                    "delete" -> {
                        hm.put("mode","delete")
                        hm.put("id_kategori",edIdkategori.text.toString())
                    }

                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onStart() {
        super.onStart()
        showDataKategori("")
    }

    fun clearInput(){
        edIdkategori.setText("")
        edNamaKategori.setText("")
    }
}