package dwi.bagas.uaskampoengjava

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_kategori.*

class AdapterKategori(val dataKategori : List<HashMap<String,String>>, val kategoriActivity: KategoriActivity) :
    RecyclerView.Adapter<AdapterKategori.HolderDataKategori>(){

    override fun onCreateViewHolder( p0: ViewGroup, p1: Int): HolderDataKategori {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_kategori,p0,false)
        return  HolderDataKategori(v)
    }

    override fun getItemCount(): Int {
        return dataKategori.size
    }

    override fun onBindViewHolder(holder: AdapterKategori.HolderDataKategori, position: Int) {
        val data = dataKategori.get(position)
        holder.txIdkategori.setText(data.get("id_kategori"))
        holder.txNamaKategori.setText(data.get("nama_kategori"))

        holder.cLayout.setOnClickListener({
            kategoriActivity.edIdkategori.setText(data.get("id_kategori"))
            kategoriActivity.edNamaKategori.setText(data.get("nama_kategori"))
        })
    }

    class HolderDataKategori(v: View) : RecyclerView.ViewHolder(v) {
        val txIdkategori = v.findViewById<TextView>(R.id.txIdkategori)
        val txNamaKategori = v.findViewById<TextView>(R.id.txNamaKategori)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayout)
    }
}