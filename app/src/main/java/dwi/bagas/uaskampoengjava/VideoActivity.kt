package dwi.bagas.uaskampoengjava

import android.content.Intent
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.media.MediaPlayer
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_video.*
import kotlinx.android.synthetic.main.activity_video.bottomNavigationView

class VideoActivity : AppCompatActivity(), View.OnClickListener, BottomNavigationView.OnNavigationItemSelectedListener {
    lateinit var db : SQLiteDatabase
    lateinit var lsadapter: ListAdapter

    override fun onClick(v: View?) {

    }

    fun getDbObject() : SQLiteDatabase {
        db = DBOpenHelper(this).writableDatabase
        return db
    }

    val daftarVideo = intArrayOf(R.raw.nasgor, R.raw.migor, R.raw.mireb)
    var posVideSkrg = 0
    lateinit var mediaPlayer: MediaPlayer
    lateinit var mediaController: MediaController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)

        mediaController = MediaController(this)
        mediaPlayer = MediaPlayer()
        mediaController.setPrevNextListeners(nextVid,prevVid)
        mediaController.setAnchorView(videoView)
        videoView.setMediaController(mediaController)
        videoSet(posVideSkrg)
        getDbObject()
    }

    override fun onStart() {
        super.onStart()
        showDataVideo()
    }
    fun showDataVideo() {
        val c: Cursor = db.rawQuery("select IDvideo as _id ,judul from video", null)
        lsadapter = SimpleCursorAdapter(
            this, R.layout.row_video, c,arrayOf("_id", "judul"), intArrayOf(R.id.idVideo,R.id.txtVideo),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        )
        lsVideo.adapter = lsadapter
    }

    var nextVid = View.OnClickListener { v:View ->
        if(posVideSkrg<(daftarVideo.size-1)){
            posVideSkrg++
        }else{
            posVideSkrg = 0
        }
        videoSet(posVideSkrg)
    }

    var prevVid = View.OnClickListener { v:View ->
        if(posVideSkrg>0){
            posVideSkrg--
        }else{
            posVideSkrg = daftarVideo.size - 1
        }
        videoSet(posVideSkrg)
    }

    fun videoSet(pos : Int){
        videoView.setVideoURI(Uri.parse("android.resource://"+packageName+"/"+daftarVideo[pos]))
    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when (p0.itemId) {
            R.id.itemMenu -> {
                var intent = Intent(this, CrudActivity::class.java)
                startActivity(intent)
            }
            R.id.itemHome -> {
                var intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
        }
        return true
    }
}