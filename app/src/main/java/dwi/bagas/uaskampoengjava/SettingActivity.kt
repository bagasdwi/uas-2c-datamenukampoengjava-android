package dwi.bagas.uaskampoengjava

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_setting.*


class SettingActivity: AppCompatActivity(), View.OnClickListener {

    var background: String = ""
    var selectedBack : String = ""
    var currentBack = ""
    var checkHD = ""
    var getTitle = ""

    lateinit var adapterSpin : ArrayAdapter<String>
    val arrayBackground = arrayOf("Putih","Biru","Kuning","Hijau","Hitam")

    lateinit var preferences: SharedPreferences
    val PREF_NAME = "setting"
    //    spinner
    val bgMain = "background"
    val DEF_BACK = "Putih"
    var checkedRadio = ""
    val TITLE = "Apa Sih Kampoeng Java Itu?"
    val CONTENT = "Content"
    val DEFF_CONTENT = "Kampoeng Java Resto and Cafe merupakan suatu usaha kuliner yang terletak di Perum Kadiri palace jalan dandang gendis kecamatan ngasem desa Gogorante Kediri yang bergerak di bidang penjualan makanan dan minuman. Kampoeng Java Resto and Cafe memiliki 5 orang pegawai dan memiliki menu khas yang diberikan adalah masakan jawa yang diolah sedemikian rupa sehingga dapat disajikan dengan menarik."
    val DEFF = "KAMPOENG JAVA"
    val FONT_SUBTITLE = "subFont"
    val DEF_FONT_SIZE = 20
    val FONT_DETAIL = "F"

    override fun onClick(v: View?) {
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val prefEditor = preferences.edit()
        prefEditor.putString(bgMain,selectedBack)
        prefEditor.putString(TITLE,txtTitle.text.toString())
        prefEditor.putInt(FONT_SUBTITLE,seekBar2.progress)
        prefEditor.putString(CONTENT,txtContent.text.toString())
        prefEditor.putInt(FONT_DETAIL,seekBar3.progress)
        prefEditor.commit()
        System.exit(0)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        currentBack = preferences.getString(bgMain,DEF_BACK).toString()
        adapterSpin = ArrayAdapter(this,android.R.layout.simple_list_item_1,arrayBackground)
        txtTitle.setText(preferences.getString(TITLE,DEFF))
        txtContent.setText(preferences.getString(CONTENT,DEFF_CONTENT))
        seekBar2.progress = preferences.getInt(FONT_SUBTITLE,DEF_FONT_SIZE)
        seekBar3.progress = preferences.getInt(FONT_DETAIL,DEF_FONT_SIZE)
        spBackground.adapter = adapterSpin
        spBackground.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                background = adapterSpin.getItem(position).toString()
                if(background.equals("Putih")){
                    selectedBack = "Putih"
                }
                if(background.equals("Biru")){
                    selectedBack = "Biru"
                }
                if(background.equals("Kuning")){
                    selectedBack = "Kuning"
                }
                if(background.equals("Hijau")){
                    selectedBack = "Hijau"
                }
                if(background.equals("Hitam")){
                    selectedBack = "Hitam"
                }
            }
        }

        btnSimpan.setOnClickListener(this)
    }



    override fun onStart() {
        super.onStart()
        spBackground.setSelection(getIndex(spBackground,currentBack))
    }

    fun getIndex(spinner: Spinner, myString: String) : Int {
        var a = spinner.count
        var b : String =""
        for(i in 0 until a){
            b = arrayBackground.get(i)
            if(b.equals(myString,ignoreCase = true)){
                return  i
            }
        }
        return 0
    }
}