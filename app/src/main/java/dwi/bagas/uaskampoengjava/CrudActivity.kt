package dwi.bagas.uaskampoengjava

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.crud_activity.*
import kotlinx.android.synthetic.main.crud_activity.bottomNavigationView

class CrudActivity : AppCompatActivity(), View.OnClickListener, BottomNavigationView.OnNavigationItemSelectedListener  {


    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnMenu ->{
                var menu = Intent(this, MenuActivity::class.java)
                startActivity(menu)
                true
            }

            R.id.btnSatuan ->{
                var satuan = Intent(this, SatuanActivity::class.java)
                startActivity(satuan)
                true
            }

            R.id.btnKategori ->{
                var kategori = Intent(this, KategoriActivity::class.java)
                startActivity(kategori)
                true
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.crud_activity)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)

        btnMenu.setOnClickListener(this)
        btnSatuan.setOnClickListener(this)
        btnKategori.setOnClickListener(this)
    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when (p0.itemId) {
            R.id.itemVideo -> {
                var intent = Intent(this, VideoActivity::class.java)
                startActivity(intent)
            }
            R.id.itemHome -> {
                var intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
        }
        return true
    }
}
