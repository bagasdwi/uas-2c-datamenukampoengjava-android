package dwi.bagas.uaskampoengjava

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context: Context): SQLiteOpenHelper(context,DB_Name,null,DB_ver) {

    companion object{
        val DB_Name = "playlist"
        val DB_ver = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val video = "create table video (IDvideo varchar(30) primary key, judul text not null)"
        val insertvideo = "insert into video values('0x7f0f0002','Nasi Goreng'),('0x7f0f0000','Mie Goreng'),('0x7f0f0001','Mie Rebus')"

        db?.execSQL(video)
        db?.execSQL(insertvideo)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("Not yet implemented")
    }

}