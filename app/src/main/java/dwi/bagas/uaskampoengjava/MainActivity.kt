package dwi.bagas.uaskampoengjava

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    lateinit var preferences: SharedPreferences
    val PREF_NAME = "setting"
    val FONT_SUBTITLE = "subFont"
    val FONT_DETAIL = "F"
    val DEF_FONT_SIZE = 20
    val DEF_TEXT = "Hello World"
    val bgMain = "background"
    val DEF_BACK = "Putih"
    val TITLE = "Apa Sih Kampoeng Java Itu?"
    val DEFF = "KAMPOENG JAVA"
    val CONTENT = "Content"
    val DEFF_CONTENT = "Kampoeng Java Resto and Cafe merupakan suatu usaha kuliner yang terletak di Perum Kadiri palace jalan dandang gendis kecamatan ngasem desa Gogorante Kediri yang bergerak di bidang penjualan makanan dan minuman. Kampoeng Java Resto and Cafe memiliki 5 orang pegawai dan memiliki menu khas yang diberikan adalah masakan jawa yang diolah sedemikian rupa sehingga dapat disajikan dengan menarik."

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var menuInflater = menuInflater
        menuInflater.inflate(R.menu.menu_option,menu)
        return super.onCreateOptionsMenu(menu)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.mnSetting -> {
                var intent = Intent(this,SettingActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun getBackground(myString: String, el:Boolean){
        if(myString.equals("Putih")){
            if (el){
                bgAct.setBackgroundColor(Color.WHITE)
                txtTitle.setTextColor(Color.BLACK)
                txtSubTitle.setTextColor(Color.BLACK)
                txtDetail.setTextColor(Color.BLACK)
            }
        }
        if(myString.equals("Hijau")){
            if (el){
                bgAct.setBackgroundColor(Color.GREEN)
                txtTitle.setTextColor(Color.BLACK)
                txtSubTitle.setTextColor(Color.BLACK)
                txtDetail.setTextColor(Color.BLACK)
            }
        }
        if(myString.equals("Biru")){
            if (el){
                bgAct.setBackgroundColor(Color.BLUE)
                txtTitle.setTextColor(Color.WHITE)
                txtSubTitle.setTextColor(Color.WHITE)
                txtDetail.setTextColor(Color.WHITE)
            }
        }
        if(myString.equals("Kuning")){
            if (el){
                bgAct.setBackgroundColor(Color.YELLOW)
                txtTitle.setTextColor(Color.BLACK)
                txtSubTitle.setTextColor(Color.BLACK)
                txtDetail.setTextColor(Color.BLACK)
            }
        }
        if(myString.equals("Hitam")){
            if (el){
                bgAct.setBackgroundColor(Color.BLACK)
                txtTitle.setTextColor(Color.WHITE)
                txtSubTitle.setTextColor(Color.WHITE)
                txtDetail.setTextColor(Color.WHITE)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)

        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val wholeBack = preferences.getString(bgMain,DEF_BACK).toString()
        getBackground(wholeBack,true)
        txtTitle.setText(preferences.getString(TITLE,DEFF))
        txtSubTitle.setTextSize(preferences.getInt(FONT_SUBTITLE,DEF_FONT_SIZE).toFloat())
        txtDetail.setText(preferences.getString(CONTENT,DEFF_CONTENT))
        txtDetail.setTextSize(preferences.getInt(FONT_DETAIL,DEF_FONT_SIZE).toFloat())
        Toast.makeText(this,"Perubahan telah disimpan.",Toast.LENGTH_SHORT).show()
    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when (p0.itemId) {
            R.id.itemMenu -> {
                var intent = Intent(this, CrudActivity::class.java)
                startActivity(intent)
            }
            R.id.itemVideo -> {
                var intent = Intent(this, VideoActivity::class.java)
                startActivity(intent)
            }
        }
        return true
    }
}