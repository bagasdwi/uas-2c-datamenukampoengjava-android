package dwi.bagas.uaskampoengjava

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_menu.*

class AdapterDataMenu(val dataMenu : List<HashMap<String,String>>, val menuaActivity: MenuActivity) :
    RecyclerView.Adapter<AdapterDataMenu.HolderDataMenu>(){
    override fun onCreateViewHolder( p0: ViewGroup, p1: Int): AdapterDataMenu.HolderDataMenu {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_menu,p0,false)
        return  HolderDataMenu(v)
    }

    override fun getItemCount(): Int {
        return dataMenu.size
    }

    override fun onBindViewHolder(p0: AdapterDataMenu.HolderDataMenu, p1: Int) {
        val data = dataMenu.get(p1)
        p0.txIdmenu.setText(data.get("id_menu"))
        p0.txNama.setText(data.get("nama"))
        p0.txSatuan.setText(data.get("nama_satuan"))
        p0.txKategori.setText(data.get("nama_kategori"))
        p0.txHarga.setText(data.get("harga"))

        p0.cLayout.setOnClickListener({
            val pos = menuaActivity.daftarSatuan.indexOf(data.get("nama_satuan"))
            val pos2 = menuaActivity.daftarKategori.indexOf(data.get("nama_kategori"))
            menuaActivity.spSatuan.setSelection(pos)
            menuaActivity.spKategori.setSelection(pos2)
            menuaActivity.edIdmenu.setText(data.get("id_menu"))
            menuaActivity.edNama.setText(data.get("nama"))
            menuaActivity.edHarga.setText(data.get("harga"))
            Picasso.get().load(data.get("url")).into(menuaActivity.imgUpload)
        })

        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photo)
    }

    class HolderDataMenu(v: View) : RecyclerView.ViewHolder(v){
        val txIdmenu = v.findViewById<TextView>(R.id.txIdmenu)
        val txNama = v.findViewById<TextView>(R.id.txNama)
        val txSatuan = v.findViewById<TextView>(R.id.txSatuan)
        val txKategori = v.findViewById<TextView>(R.id.txKategori)
        val photo = v.findViewById<ImageView>(R.id.imageView)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayout)
        val txHarga = v.findViewById<TextView>(R.id.txHarga)
    }
}