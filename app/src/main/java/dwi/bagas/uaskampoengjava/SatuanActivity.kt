package dwi.bagas.uaskampoengjava

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_satuan.*
import kotlinx.android.synthetic.main.activity_satuan.btnDelete
import kotlinx.android.synthetic.main.activity_satuan.btnFind
import kotlinx.android.synthetic.main.activity_satuan.btnInsert
import kotlinx.android.synthetic.main.activity_satuan.btnUpdate
import org.json.JSONArray
import org.json.JSONObject
import kotlin.collections.HashMap

class SatuanActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var satuanAdapter : AdapterSatuan
    var daftarSatuan = mutableListOf<HashMap<String,String>>()
    var idSatuan = ""
    val mainUrl = "http://192.168.43.94/uas_android/"
    val url = mainUrl+"get_nama_satuan.php"
    val url2 = mainUrl+"query_crud_satuan.php"

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsert ->{
                querySatuan("insert")
            }
            R.id.btnUpdate ->{
                querySatuan("update")
            }
            R.id.btnDelete ->{
                querySatuan("delete")
            }
            R.id.btnFind -> {
                showDataSatuan(edNamaSatuan.text.toString())
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_satuan)
        satuanAdapter = AdapterSatuan(daftarSatuan,this)
        listSatuan.layoutManager = LinearLayoutManager(this)
        listSatuan.adapter = satuanAdapter
        btnInsert.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        btnFind.setOnClickListener(this)
    }

    fun showDataSatuan(namaSatuan : String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarSatuan.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var satuan = HashMap<String,String>()
                    satuan.put("id_satuan",jsonObject.getString("id_satuan"))
                    satuan.put("nama_satuan",jsonObject.getString("nama_satuan"))
                    daftarSatuan.add(satuan)
                }
                satuanAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("nama_satuan",namaSatuan)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun querySatuan(mode : String){
        val request = object : StringRequest(
            Method.POST,url2,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil", Toast.LENGTH_LONG).show()
                    showDataSatuan("")
                    clearInput()
                }else{
                    Toast.makeText(this,"Operasi Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "update" -> {
                        hm.put("mode","update")
                        hm.put("id_satuan",edIdsatuan.text.toString())
                        hm.put("nama_satuan",edNamaSatuan.text.toString())
                    }
                    "insert" -> {
                        hm.put("mode","insert")
                        hm.put("id_satuan",edIdsatuan.text.toString())
                        hm.put("nama_satuan",edNamaSatuan.text.toString())
                    }
                    "delete" -> {
                        hm.put("mode","delete")
                        hm.put("id_satuan",edIdsatuan.text.toString())
                    }

                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onStart() {
        super.onStart()
        showDataSatuan("")
    }

    fun clearInput(){
        edIdsatuan.setText("")
        edNamaSatuan.setText("")
    }
}